/*! \file 4011-TestButtons.c
 *
 *  \brief Chesk that buttons are working
 *
 * Author: jjmcd
 *
 * Created on September 6, 2012, 8:43 PM
 */

#include <p30Fxxxx.h>

//! main - Test that buttons are working

/*! All LED are turned off
 * If the UP button is pressed, the top LED is illuminated
 * If the DOWN button is pressed, the bottom LED is illuminated
 * If the LEFT button is pressed, the center LED is illuminated
 * If the RIGHT button is pressed, the outer LEDs are illuminated
 *
 * Buttons are normally open, pulled up, so they are FALSE when
 * depressed, TRUE otherwise.  Similarly, LEDs are pulled up so a
 * low on the output pin turns on the LED
 */
int main (void)
{
  int i;

  LATD |= 0x000e;       // Initially set LEDs off
  TRISD &= 0xfff1;      // LED pins to outputs

  while (1)             // It's going to take a while before 1
    {                   // quits being 1
      LATD |= 0x000e;   // All LEDs off
      _LATD2 = _RE0;    // Top LED follows UP button
      _LATD1 = _RE1;    // Bottom LED follows DOWN button
      _LATD3 = _RE2;    // Center LED follows LEFT button
      if ( !_RE3 )      // RIGHT pressed?
        {               // Yes
          _LATD2 = 0;   // Illuminate top LED
          _LATD1 = 0;   // Illuminate bottom LED
        }               // end if
    }                   // end while
}                       // end main()



